#include "ToyThrow.h"


ToyThrow::ToyThrow()
{
  //number of toy throws
  NTHROWS=0;

  //initialize fit parameters to zero
  for(int t = 0; t < NTARG; t++){
    for(int i = 0; i < NPAR; i++){
      pars[t][i] = -9999;
    }
  }

  //default error and weight sets
  WeightType = -9999;
  ErrorType  = -9999;


  //if you name these histograms then they get explicit
  //locations in root's memory and overwrite each other
  double nxbins = 60;
  double xmax   = 6;
  double xmin   = 0;
  double nybins = 15;
  double ymin   = -0.5;
  double ymax   = 14.5; 
  h2d_lnw2_nchpi[kProton] = new TH2D("", "Proton;Log W^{2};Charged Pion <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_nchhad[kProton]= new TH2D("", "Proton;Log W^{2};Charged Hadron <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_npim[kProton]  = new TH2D("", "Proton;Log W^{2};Pi-minus <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_npip[kProton]  = new TH2D("", "Proton;Log W^{2};Pi-plus <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_npi0[kProton]  = new TH2D("", "Proton;Log W^{2};Pi-zero <n>",nxbins,xmin,xmax,nybins,ymin,ymax);

  h2d_lnw2_nchpi[kNeutron] = new TH2D("", "Neutron;Log W^{2};Charged Pion <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_nchhad[kNeutron]= new TH2D("", "Neutron;Log W^{2};Charged Hadron <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_npim[kNeutron]  = new TH2D("", "Neutron;Log W^{2};Pi-minus <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_npip[kNeutron]  = new TH2D("", "Neutron;Log W^{2};Pi-plus <n>",nxbins,xmin,xmax,nybins,ymin,ymax);
  h2d_lnw2_npi0[kNeutron]  = new TH2D("", "Neutron;Log W^{2};Pi-zero <n>",nxbins,xmin,xmax,nybins,ymin,ymax);

  //consider initialization of random generator
  R = 0;

  //levy functions for sampling
  levy[kProton]  = new TF1("","2*TMath::Exp(-[0])*TMath::Power([0], [0]*x+1)/TMath::Gamma([0]*x+1)",0,5);
  levy[kProton]->SetParameter(kC, pars[kProton][kC]);
  levy[kNeutron] = new TF1("","2*TMath::Exp(-[0])*TMath::Power([0], [0]*x+1)/TMath::Gamma([0]*x+1)",0,5);
  levy[kNeutron]->SetParameter(kC, pars[kNeutron][kC]);


}

ToyThrow::~ToyThrow(){

  for(int i = 0; i < NTARG; i++){
    delete h2d_lnw2_nchhad[i];
    delete h2d_lnw2_nchpi[i];
    delete h2d_lnw2_npim[i];
    delete h2d_lnw2_npip[i];
    delete h2d_lnw2_npi0[i];
    delete levy[i];
  }

}

void ToyThrow::CheckReady(){

  for(int t = 0; t < NTARG; t++){

    if(NTHROWS==0){
      std::cout << "Set the number of throws for toy data generation!" << std::endl;
      exit(EXIT_FAILURE);
    }
    //check levy function readiness
    if(levy[t]->GetParameter(0)==-9999){
      std::cout << "Levy Function Parameter Not Initialized!" << std::endl;
      exit(EXIT_FAILURE);
    }

    //check throw parameters readiness
    for(int i = 0; i < NPAR; i++){
      if(pars[t][i]==-9999){
      std::cout << "Parameters Not Initialized Correctly!" << std::endl;
      exit(EXIT_FAILURE);
      }
    }

  }

    if(R==0){
      std::cout << "Random number generator not set!" << std::endl;
      exit(EXIT_FAILURE);
    }
    if(WeightType==-9999){
      std::cout << "Weight Category not set!" << std::endl;
      exit(EXIT_FAILURE);
    }
    if(ErrorType==-9999){
      std::cout << "Error Category not set!" << std::endl;
      exit(EXIT_FAILURE);
    }

    return;
}
void ToyThrow::PrintParameters(){
  std::cout <<"\n";
  std::cout <<"Proton Parameters, A = " << GetA_p() << ", B = " << GetB_p();
  std::cout <<"C = " << GetLevyC_p() << ", Alpha = " << GetAlpha_p() << "\n";
  std::cout <<"Neutron Parameters, A = " << GetA_n() << ", B = " << GetB_n();
  std::cout <<"C = " << GetLevyC_n() << ", Alpha = " << GetAlpha_n() << "\n";
  return;
}

void ToyThrow::GenerateDataSet(){
  
  CheckReady();

  //use some short-hand variables for this routine
  double Alpha_p = pars[kProton][kAlpha];
  double A_p     = pars[kProton][kA];
  double B_p     = pars[kProton][kB];
  double Alpha_n = pars[kNeutron][kAlpha];
  double A_n     = pars[kNeutron][kA];
  double B_n     = pars[kNeutron][kB];
  double alpha   = 0;
  for(int rand=0; rand < NTHROWS; rand++){
    double w = R->Rndm()*14+1.3;

    //select target nucleon, proton or neutron
    double neutral_or_charged = R->Rndm();
    if(neutral_or_charged>0.5)
      neutral_or_charged=1.0;//charged, nu-p interaction
    else
      neutral_or_charged=0.0;//neutral,nu-n interaction


    double mean =0;
    if(neutral_or_charged == 1){//proton
      mean = 1.5*(-0.66 + A_p + B_p*TMath::Log(w*w));
      alpha= Alpha_p;
    }
    else{//neutron
      //mean = 1.5*(-0.50-0.19+1.42*TMath::Log(w**2));
      mean = 1.5*(-0.50 + A_n + B_n*TMath::Log(w*w));
      alpha= Alpha_n;
    }

    double m_nucleon = 0.939;//approx nucleon mass                              
    double m_pi = 0.140;//approx charged pion mass                              
    //invariant hadronic mass is attributed to one                              
    //ejected nucleon and then some number of pions,                            
    //so make sure you don't select a pion multiplicity                         
    //larger than the permissible number -- by construction                     
    //this should be very rare (KNO distribution is small upper tail)           
    int kinematicLimit = (int)((w-m_nucleon)/m_pi); 

    int multiplicity=0;
    double z = 0;
    for(int a=0; a<400; a++){
      if(neutral_or_charged == 1)
        z = levy[kProton]->GetRandom();
      else
        z = levy[kNeutron]->GetRandom();

      //this is a trick to round a number to the nearest
      //integer: casting to int will always floor the double,
      //so add 0.5 first and if the double is in the upper 
      //half of the decimal it will get floored "up", otherwise
      //it will round down as desired. Without this adjustment
      //there is a slight bias in the sampling.
      multiplicity=(int)( (mean+alpha)*z - alpha +0.5);//floor the double with cast to int
      if(multiplicity > kinematicLimit){                                        
        //std::cout << "Multiplicity exceeds kinematic limit! Rethrow!\n";        
        continue;                                                               
      }

      if(w>2 && multiplicity >=1)                                               
        break;                                                                  
      else if(multiplicity>=2)                                                  
        break;                                                                  
    }                                                                           

    if(w<2 && multiplicity <2)                                               
      continue;                                                                  
    else if(w>2 && multiplicity<1)                                                  
      continue;                                                                  

    //std::cout << "multiplicity = " << multiplicity << std::endl;

    int charged_multiplicity=0;

    //std::cout << "target = " << neutral_or_charged<< std::endl;
    //testing, set to proton target
    //neutral_or_charged=1;

    double ejected_nucleon = R->Rndm();
    if(ejected_nucleon>0.5)
      ejected_nucleon=1.0;//proton ejected
    else
      ejected_nucleon=0.0;//neutron ejected

    //std::cout << "ejected = " << ejected_nucleon<< std::endl;
    //std::cout << "Event is on proton = " << neutral_or_charged << std::endl;
    //std::cout << "Event multiplicity = " << multiplicity<< std::endl;

    double toss = 0;
    double plus_charge=0;
    double minus_charge=0;
    double neutral_pi =0;
    double mult = multiplicity;

    //need total charge to be zero, subtract 1 for outgoing muon,
    //subtract off the target nucleon charge
    double total_charge = ejected_nucleon - 1 - neutral_or_charged;
    
    //std::cout << "total charge = " << total_charge<< std::endl;
    //first balance the charge with necessary 
    //charged pions, subtract count from total multiplicity
    if(total_charge < 0){
      charged_multiplicity= charged_multiplicity - total_charge;
      plus_charge = plus_charge - total_charge;
      multiplicity = multiplicity + total_charge;
    }
    else if(total_charge > 0){
      charged_multiplicity= charged_multiplicity + total_charge;
      minus_charge = minus_charge + total_charge;
      multiplicity = multiplicity - total_charge;
    }
    //std::cout << "multiplicity after charge balance  = " << multiplicity << std::endl;
    //std::cout << "charged_multiplicity after charge balance  = " << charged_multiplicity<< std::endl;
    //std::cout << "plus_charge after charge balance  = " << plus_charge << std::endl;
    //std::cout << "minus_charge after charge balance  = " << minus_charge << std::endl;
    //std::cout << "neutral_pi after charge balance  = " << neutral_pi<< std::endl;

    //if remaining multiplicity is odd, add one neutral 
    //pion, subtract one from remaining multiplicity
    if(multiplicity%2 == 1){
      neutral_pi++;
      multiplicity = multiplicity - 1;
    }
    //std::cout << "multiplicity after even-odd balance  = " << multiplicity << std::endl;
    //std::cout << "charged_multiplicity after even-odd balance  = " << charged_multiplicity<< std::endl;
    //std::cout << "plus_charge after even-odd balance  = " << plus_charge << std::endl;
    //std::cout << "minus_charge after even-odd balance  = " << minus_charge << std::endl;
    //std::cout << "neutral_pi after even-odd balance  = " << neutral_pi<< std::endl;

    //for remaining multiplicity, generate neutral pairs 
    //of pions
    multiplicity = multiplicity/2;
    for(int i = 0; i < multiplicity; i++){
      toss = R->Rndm();
      if(toss < (1.0/3.0)){
        neutral_pi = neutral_pi + 2;
      }
      else{
        charged_multiplicity = charged_multiplicity + 2;
        plus_charge++;
        minus_charge++;
      }
    }

    multiplicity=mult;
    if(multiplicity==1){                                                        
    //proton target has only one option for multiplicity                      
    //of one                                                                  
      if(neutral_or_charged == 1){                                              
        charged_multiplicity=1;                                                 
        plus_charge    =1;                                                      
        neutral_pi     =0;                                                      
        minus_charge   =0;                                                      
        ejected_nucleon=neutral_or_charged;                                     
      }                                                                         
      else{                                                                     
        //neutron target has two outcomes, dictated by ejected nucleon          
        if(ejected_nucleon == 1){//proton ejected                               
          charged_multiplicity=0;                                               
          plus_charge    =0;                                                    
          neutral_pi     =1;                                                    
          minus_charge   =0;                                                    
        }                                                                       
        else{//neutron ejected                                                  
          charged_multiplicity=1;                                               
          plus_charge    =1;                                                    
          neutral_pi     =0;                                                    
          minus_charge   =0;                                                    
        }                                                                       
      }                                                                         
    }  
  

    if(multiplicity != plus_charge + minus_charge + neutral_pi){
      std::cout << "multiplicity mismatch! quitting!" << std::endl;
      std::cout << "Mult = " << mult << std::endl;
      std::cout << "target= " << neutral_or_charged<< std::endl;
      std::cout << "ejected= " << ejected_nucleon<< std::endl;
      std::cout << "total charge= " << total_charge<< std::endl;
      std::cout << "pi+ = " << plus_charge<< std::endl;
      std::cout << "pi- = " << minus_charge<< std::endl;
      std::cout << "pi0 = " << neutral_pi<< std::endl;
      std::cout << "charge pi = " << charged_multiplicity<< std::endl;
      return;
    }


      

    double w2 = w*w;
    double lnw2 = TMath::Log(w2);
    
    if(neutral_or_charged){//proton
      h2d_lnw2_nchpi[kProton]->Fill(lnw2,charged_multiplicity);
      h2d_lnw2_nchhad[kProton]->Fill(lnw2,charged_multiplicity+ejected_nucleon);
      h2d_lnw2_npim[kProton]->Fill(lnw2,plus_charge);
      h2d_lnw2_npip[kProton]->Fill(lnw2,minus_charge);
      h2d_lnw2_npi0[kProton]->Fill(lnw2,neutral_pi);
    }
    else{//neutron
      h2d_lnw2_nchpi[kNeutron]->Fill(lnw2,charged_multiplicity);
      h2d_lnw2_nchhad[kNeutron]->Fill(lnw2,charged_multiplicity+ejected_nucleon);
      h2d_lnw2_npim[kNeutron]->Fill(lnw2,plus_charge);
      h2d_lnw2_npip[kNeutron]->Fill(lnw2,minus_charge);
      h2d_lnw2_npi0[kNeutron]->Fill(lnw2,neutral_pi);
    }
  }
  return;

}

void ToyThrow::Reset(){
  
  //resets histogram content without deleting histogram objects
  //resets parameter values
  //can only be called after histos have been created, so make sure
  //it is only called after initialization at least once:
  CheckReady();

  //reset histogram content
  for(int t = 0; t < NTARG; t++){
    h2d_lnw2_nchpi[t]->Reset();
    h2d_lnw2_nchhad[t]->Reset();
    h2d_lnw2_npim[t]->Reset();
    h2d_lnw2_npip[t]->Reset();
    h2d_lnw2_npi0[t]->Reset();

    //reset parameters
    for(int p = 0; p < NPAR; p++){
      pars[t][p] = -9999;
    }
  }
  return;
}


