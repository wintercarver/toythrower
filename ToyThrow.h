#ifndef TOYTHROW_H                                                                
#define TOYTHROW_H
#include<TH2D.h>
#include<TRandom.h>
#include<TMath.h>
#include<TF1.h>
class ToyThrow{
  public:
    //parameter get functions
    //protons
    double GetA_p() {return pars[kProton][kA];}
    double GetB_p() {return pars[kProton][kB];}
    double GetAlpha_p() {return pars[kProton][kAlpha];}
    double GetLevyC_p() {return pars[kProton][kC];}
    //neutrons
    double GetA_n() {return pars[kNeutron][kA];}
    double GetB_n() {return pars[kNeutron][kB];}
    double GetAlpha_n() {return pars[kNeutron][kAlpha];}
    double GetLevyC_n() {return pars[kNeutron][kC];}

    //general set function
    void SetPar(int targ, int par, double val) {pars[targ][par]=val;}
    //proton
    void SetA_p(double val) {pars[kProton][kA]=val;}
    void SetB_p(double val){pars[kProton][kB]=val;}
    void SetAlpha_p(double val){pars[kProton][kAlpha]=val;}
    void SetLevyC_p(double val){pars[kProton][kC]=val; levy[kProton]->SetParameter(kC,val);}
    //neutron
    void SetA_n(double val) {pars[kNeutron][kA]=val;}
    void SetB_n(double val){pars[kNeutron][kB]=val;}
    void SetAlpha_n(double val){pars[kNeutron][kAlpha]=val;}
    void SetLevyC_n(double val){pars[kNeutron][kC]=val; levy[kNeutron]->SetParameter(kC,val);}

    //random number generator
    void SetRandomEngine(TRandom* engine){R=engine;}
    
    //set weight and error categories
    void SetWeightType(int type){WeightType=type;}
    void SetErrorType(int type){ErrorType=type;}

    //set number of throws
    void SetNThrows(int n){NTHROWS = n;}

    //histogram get functions
    TH2D* GetProtonLogW2ChPi()   {return h2d_lnw2_nchpi[kProton];}
    TH2D* GetProtonLogW2ChHad()  {return h2d_lnw2_nchhad[kProton];}
    TH2D* GetProtonLogW2Pim()    {return h2d_lnw2_npim[kProton];}
    TH2D* GetProtonLogW2Pip()    {return h2d_lnw2_npip[kProton];}
    TH2D* GetProtonLogW2Pi0()    {return h2d_lnw2_npi0[kProton];}

    TH2D* GetNeutronLogW2ChPi()  {return h2d_lnw2_nchpi[kNeutron];}
    TH2D* GetNeutronLogW2ChHad() {return h2d_lnw2_nchhad[kNeutron];}
    TH2D* GetNeutronLogW2Pim()   {return h2d_lnw2_npim[kNeutron];}
    TH2D* GetNeutronLogW2Pip()   {return h2d_lnw2_npip[kNeutron];}
    TH2D* GetNeutronLogW2Pi0()   {return h2d_lnw2_npi0[kNeutron];}

    //print info
    void PrintParameters();

    void CheckReady();
    void GenerateDataSet();
    void Reset();

    //dtor and ctor
    ToyThrow();
    ~ToyThrow();


  private:

    //number of throws
    int    NTHROWS;
    
    //categories of throwing 
    int    WeightType;//0=All Weights, 1=BinCount>5, 2=Weights<10
    int    ErrorType;//0=Statistical Errors from fermilab, 1=Stat+systematic

    enum   TARGET {kNeutron=0, kProton};
    static const int NTARG = 2;
    TH2D*  h2d_lnw2_nchpi[NTARG];  //charged pions
    TH2D*  h2d_lnw2_nchhad[NTARG]; //chared hadrons (ch.pi+proton_
    TH2D*  h2d_lnw2_npim[NTARG];   //pi minus 
    TH2D*  h2d_lnw2_npip[NTARG];   //pi plus
    TH2D*  h2d_lnw2_npi0[NTARG];   //pi zero

    //fit parameters, used to control dynamics of toy throwing
    enum PARS {kC = 0, kA, kB, kAlpha};
    static const int NPAR = 4;
    double pars[NTARG][NPAR];
    
    //pointer to random number generator, must be set
    TRandom *R;

    //levy functions
    TF1* levy[NTARG];

};

#endif

