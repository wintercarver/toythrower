//build with 
//g++ -o thrower.exe thrower.cpp ToyThrower.cpp `root-config --cflags --libs`
#include <iostream>
#include <stdlib.h>
#include <TStopwatch.h>
#include <TRandom.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TFile.h>
#include <TF2.h>
#include "ToyThrow.h"

#ifdef _OPENMP
#include <omp.h>
#endif

void InitializeParameters(ToyThrow* aThrow, double pars_p[], double pars_n[]);


void benchmark(char* ntoys, char* iterations){
  //start timer for program execution
  TStopwatch timer;
  timer.Start();

  //get factor -- default is 1 billion throws
  int NTHROWS;
  int NTOYS;
  //check number of toys, default is 10
  if(ntoys)
    NTOYS = atoi(ntoys);
  else
    NTOYS = 10;

  //check number of iterations per toy
  if(iterations)
    NTHROWS = atoi(iterations);
  else
    NTHROWS = 1000000;

  std::cout << "NTOYS = " << NTOYS <<", NTHROWS = " << NTHROWS << std::endl;

  //random number engine
  TRandom* R = new TRandom();
  R->SetSeed(7654321);

  //input parameter file of correlated parameter throws
  TFile* paramFile = new TFile("./inputs/correlated_throws_3D.root","READ");
  TTree* cor_throws= (TTree*) paramFile->Get("cor_throws");
  if(NTOYS > cor_throws->GetEntries()){
    NTOYS = cor_throws->GetEntries();
    std::cout << "NTOYS greater than available number of thrown parameters.\n";
    std::cout << "Reseting value to maximum available, NTOYS = " << NTOYS << std::endl;
  }

  std::cout << "\nBeginning toy generation: " << NTOYS << " toy data sets with " << NTHROWS << " throws per toy.\n";

  const int NCAT  = 3;//fixed, free
  enum CATEGORY {kFixed=0, kFree, kHybrid};
  char SCAT[NCAT][50] = {"fixed","free","hybrid"};

  const int NTARG = 2;//proton, neutron
  enum TARGET   {kNeutron=0, kProton};
  char STARG[NTARG][50] = {"n","p"};

  const int NERR = 2;//statistical error, stat+syst error
  enum ERR   {kStat=0, kTot};
  char SERR[NERR][50] = {"stat","tot"};

  const int NPAR  = 4;//four fit parameters: LevyC, A, B, Alpha
  enum PARAMETER{kC=0, kA, kB, kAlpha};


  double pars[NCAT][NTARG][NPAR];//stat error only
  double pars_tot[NCAT][NTARG][NPAR];//stat+syst error
  cor_throws->SetBranchAddress("par_free_proton",    pars[kFree][kProton]);
  cor_throws->SetBranchAddress("par_free_neutron",   pars[kFree][kNeutron]);
  cor_throws->SetBranchAddress("par_fixed_proton",   pars[kFixed][kProton]);
  cor_throws->SetBranchAddress("par_fixed_neutron",  pars[kFixed][kNeutron]);
  cor_throws->SetBranchAddress("par_hybrid_proton",  pars[kHybrid][kProton]);
  cor_throws->SetBranchAddress("par_hybrid_neutron", pars[kHybrid][kNeutron]);

  cor_throws->SetBranchAddress("par_free_proton_tot",    pars_tot[kFree][kProton]);
  cor_throws->SetBranchAddress("par_free_neutron_tot",   pars_tot[kFree][kNeutron]);
  cor_throws->SetBranchAddress("par_fixed_proton_tot",   pars_tot[kFixed][kProton]);
  cor_throws->SetBranchAddress("par_fixed_neutron_tot",  pars_tot[kFixed][kNeutron]);
  cor_throws->SetBranchAddress("par_hybrid_proton_tot",  pars_tot[kHybrid][kProton]);
  cor_throws->SetBranchAddress("par_hybrid_neutron_tot", pars_tot[kHybrid][kNeutron]);

  //array of toys
  //ToyThrow* toys[NCAT][NTOYS];
  ToyThrow* toys[NCAT];
  ToyThrow* toys_tot[NCAT];

  //initialize toy parameters
  //dummy initialization 
  for(int c = 0; c < NCAT; c++){
    toys[c] = new ToyThrow();
    InitializeParameters(toys[c], pars[0][0], pars[0][0]);
    toys[c]->SetRandomEngine(R);
    toys[c]->SetNThrows(NTHROWS);
    toys[c]->SetWeightType(0);
    toys[c]->SetErrorType(0);

    toys_tot[c] = new ToyThrow();
    InitializeParameters(toys_tot[c], pars_tot[0][0], pars_tot[0][0]);
    toys_tot[c]->SetRandomEngine(R);
    toys_tot[c]->SetNThrows(NTHROWS);
    toys_tot[c]->SetWeightType(0);
    toys_tot[c]->SetErrorType(0);
  }

  TFile* output = new TFile("./outputs/thrown_histograms.root","RECREATE");
  //generate toys. OMP command seems to do nothing? I'm an idiot?
  //#pragma omp parallel for num_threads(4) private(n) -- this seems to do nothing
  //#pragma omp parallel for schedule(dynamic, 2)
  for(int n = 0; n < NTOYS; n++){
    std::cout << "Generating Toy Data: " << n << "/" << NTOYS << std::endl;
    cor_throws->GetEntry(n);
    for(int c = 0; c < NCAT; c++){
      toys[c]->Reset();
      toys_tot[c]->Reset();
      InitializeParameters(toys[c], pars[c][kProton], pars[c][kNeutron]);
      InitializeParameters(toys_tot[c], pars_tot[c][kProton], pars_tot[c][kNeutron]);
      std::cout << "Proton\n";
      std::cout << pars[c][kProton][0] << " " << pars_tot[c][kProton][0] << std::endl;
      std::cout << pars[c][kProton][1] << " " << pars_tot[c][kProton][1] << std::endl;
      std::cout << pars[c][kProton][2] << " " << pars_tot[c][kProton][2] << std::endl;
      std::cout << pars[c][kProton][3] << " " << pars_tot[c][kProton][3] << std::endl;
      std::cout << "Neutron\n";
      std::cout << pars[c][kNeutron][0] << " " << pars_tot[c][kNeutron][0] << std::endl;
      std::cout << pars[c][kNeutron][1] << " " << pars_tot[c][kNeutron][1] << std::endl;
      std::cout << pars[c][kNeutron][2] << " " << pars_tot[c][kNeutron][2] << std::endl;
      std::cout << pars[c][kNeutron][3] << " " << pars_tot[c][kNeutron][3] << std::endl;
    }

    for(int c = 0; c < NCAT; c++){
      toys[c]->GenerateDataSet();
      toys[c]->GetProtonLogW2ChPi()->Write(Form("h2d_lnw2_nchpi_p_%s_%i",SCAT[c],n));
      toys[c]->GetProtonLogW2ChHad()->Write(Form("h2d_lnw2_nchhad_p_%s_%i",SCAT[c],n));
      toys[c]->GetProtonLogW2Pip()->Write(Form("h2d_lnw2_npip_p_%s_%i",SCAT[c],n));
      toys[c]->GetProtonLogW2Pim()->Write(Form("h2d_lnw2_npim_p_%s_%i",SCAT[c],n));
      toys[c]->GetProtonLogW2Pi0()->Write(Form("h2d_lnw2_npi0_p_%s_%i",SCAT[c],n));

      toys[c]->GetNeutronLogW2ChPi()->Write(Form("h2d_lnw2_nchpi_n_%s_%i",SCAT[c],n));
      toys[c]->GetNeutronLogW2ChHad()->Write(Form("h2d_lnw2_nchhad_n_%s_%i",SCAT[c],n));
      toys[c]->GetNeutronLogW2Pip()->Write(Form("h2d_lnw2_npip_n_%s_%i",SCAT[c],n));
      toys[c]->GetNeutronLogW2Pim()->Write(Form("h2d_lnw2_npim_n_%s_%i",SCAT[c],n));
      toys[c]->GetNeutronLogW2Pi0()->Write(Form("h2d_lnw2_npi0_n_%s_%i",SCAT[c],n));
    }

    //only interested in examining impact of total error
    //in the fixed-parameter set, for now.
    toys_tot[kFixed]->GenerateDataSet();
    toys_tot[kFixed]->GetProtonLogW2ChPi()->Write(Form("h2d_lnw2_nchpi_p_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetProtonLogW2ChHad()->Write(Form("h2d_lnw2_nchhad_p_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetProtonLogW2Pip()->Write(Form("h2d_lnw2_npip_p_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetProtonLogW2Pim()->Write(Form("h2d_lnw2_npim_p_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetProtonLogW2Pi0()->Write(Form("h2d_lnw2_npi0_p_%s_tot_%i",SCAT[kFixed],n));

    toys_tot[kFixed]->GetNeutronLogW2ChPi()->Write(Form("h2d_lnw2_nchpi_n_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetNeutronLogW2ChHad()->Write(Form("h2d_lnw2_nchhad_n_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetNeutronLogW2Pip()->Write(Form("h2d_lnw2_npip_n_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetNeutronLogW2Pim()->Write(Form("h2d_lnw2_npim_n_%s_tot_%i",SCAT[kFixed],n));
    toys_tot[kFixed]->GetNeutronLogW2Pi0()->Write(Form("h2d_lnw2_npi0_n_%s_tot_%i",SCAT[kFixed],n));
  }

  //generate nominal data sets from best fit parameters in joint fits
  TF2* fit_result[NTARG][NCAT];
  fit_result[kProton][kFixed]   = (TF2*)paramFile->Get("fit_result_proton_fixed");       
  fit_result[kProton][kFree]    = (TF2*)paramFile->Get("fit_result_proton_free");       
  fit_result[kProton][kHybrid]  = (TF2*)paramFile->Get("fit_result_proton_free");       
  fit_result[kNeutron][kFixed]  = (TF2*)paramFile->Get("fit_result_neutron_fixed");       
  fit_result[kNeutron][kFree]   = (TF2*)paramFile->Get("fit_result_neutron_free");       
  fit_result[kNeutron][kHybrid] = (TF2*)paramFile->Get("fit_result_neutron_free");       
  
  //the hybrid fit result is just the joint free-alpha fit results
  //for A, B and LevyC, but with alpha then re-fixed to zero. 
  fit_result[kProton][kHybrid]->SetParameter(kAlpha,0);
  fit_result[kProton][kHybrid]->SetName("fit_result_proton_hybrid");
  fit_result[kNeutron][kHybrid]->SetParameter(kAlpha,0);
  fit_result[kNeutron][kHybrid]->SetName("fit_result_neutron_hybrid");


  //this stores all the nominal fit params in a single array
  double nominalFitParameters[NCAT][NTARG][NPAR];
  for(int t = 0; t < NTARG; t++)
    for(int c = 0; c < NCAT; c++)
      for(int p = 0; p < NPAR; p++)
        nominalFitParameters[c][t][p] = fit_result[t][c]->GetParameter(p);

  //nominal toy throws
  ToyThrow* NominalModels[NCAT];
  
  //initialize nominal throws with parameters
  for(int c = 0; c < NCAT; c++){
    NominalModels[c] = new ToyThrow();
    InitializeParameters(NominalModels[c], nominalFitParameters[c][kProton], nominalFitParameters[c][kNeutron]);
    NominalModels[c]->SetRandomEngine(R);
    NominalModels[c]->SetNThrows(NTHROWS);
    NominalModels[c]->SetWeightType(0);
    NominalModels[c]->SetErrorType(0);
  }
  std::cout << "Alpha Fixed:" << std::endl;
  NominalModels[kFixed]->PrintParameters();
  std::cout << "Alpha Free:" << std::endl;
  NominalModels[kFree]->PrintParameters();
  std::cout << "Alpha Hybrid:" << std::endl;
  NominalModels[kHybrid]->PrintParameters();

  //generate nominal distributions
  NominalModels[kFixed]->GenerateDataSet();
  NominalModels[kFree]->GenerateDataSet();
  NominalModels[kHybrid]->GenerateDataSet();

  for(int c = 0; c < NCAT; c++){
    NominalModels[c]->GetProtonLogW2ChPi()->Write(Form("h2d_lnw2_nchpi_p_nom_%s",SCAT[c]));
    NominalModels[c]->GetProtonLogW2ChHad()->Write(Form("h2d_lnw2_nchhad_p_nom_%s",SCAT[c]));
    NominalModels[c]->GetProtonLogW2Pip()->Write(Form("h2d_lnw2_npip_p_nom_%s",SCAT[c]));
    NominalModels[c]->GetProtonLogW2Pim()->Write(Form("h2d_lnw2_npim_p_nom_%s",SCAT[c]));
    NominalModels[c]->GetProtonLogW2Pi0()->Write(Form("h2d_lnw2_npi0_p_nom_%s",SCAT[c]));

    NominalModels[c]->GetNeutronLogW2ChPi()->Write(Form("h2d_lnw2_nchpi_n_nom_%s",SCAT[c]));
    NominalModels[c]->GetNeutronLogW2ChHad()->Write(Form("h2d_lnw2_nchhad_n_nom_%s",SCAT[c]));
    NominalModels[c]->GetNeutronLogW2Pip()->Write(Form("h2d_lnw2_npip_n_nom_%s",SCAT[c]));
    NominalModels[c]->GetNeutronLogW2Pim()->Write(Form("h2d_lnw2_npim_n_nom_%s",SCAT[c]));
    NominalModels[c]->GetNeutronLogW2Pi0()->Write(Form("h2d_lnw2_npi0_n_nom_%s",SCAT[c]));
  }
  //save nominal fit results for records
  for(int t = 0; t < NTARG; t++)
    for(int c = 0; c < NCAT; c++)
      fit_result[t][c]->Write();

  //clone parameter throws to file to keep for records
  cor_throws->CloneTree()->Write();
  output->Close();

  timer.Stop();
  std::cout << "Finished " << NTOYS << " toy experiments in: " <<  std::endl;
  std::cout << "CPU Time  = " << timer.CpuTime() << std::endl;
  std::cout << "Real Time = " << timer.RealTime() << std::endl;
  timer.Reset();


  delete R;
  return;

}

#ifndef __CINT__ 
int main(int argc, char** argv) {
  
  std::cout << "\n********************\n";
  std::cout << "*Starting Toy Thrower*";
  std::cout << "\n********************\n";
  benchmark(argv[1], argv[2]);
  return 0;
}

void InitializeParameters(ToyThrow* aThrow, double pars_p[], double pars_n[]){

  aThrow->SetLevyC_p(pars_p[0]);
  aThrow->SetA_p(pars_p[1]);
  aThrow->SetB_p(pars_p[2]);
  aThrow->SetAlpha_p(pars_p[3]);

  aThrow->SetLevyC_n(pars_n[0]);
  aThrow->SetA_n(pars_n[1]);
  aThrow->SetB_n(pars_n[2]);
  aThrow->SetAlpha_n(pars_n[3]);

}
#endif


